import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario.model';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { UsuarioService } from 'src/app/services/usuario.service';

import * as Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {

  public perfilForm!: FormGroup;
  public usuario: Usuario;
  public imagenSubir!: File;
  public imgTemp!: any;

  constructor(
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private fileUploadService: FileUploadService
  ) { 
    this.usuario = this.usuarioService.usuario;
  }

  ngOnInit(): void {
    this.perfilForm = this.fb.group({
      nombre: [this.usuario.nombre, Validators.required],
      email: [this.usuario.email, [Validators.required, Validators.email]]
    });
  }

  actualizarPerfil(){
    this.usuarioService.actualizarPerfil(this.perfilForm.value)
          .subscribe(() => {
            const {nombre, email} = this.perfilForm.value;
            this.usuario.nombre =  nombre;
            this.usuario.email = email;

            Swal.default.fire(
              {
                position: 'center',
                icon: 'success',
                title: 'Usuario actualizada correctamente',
                showConfirmButton: false,
                timer: 2850
              }
            );
          }, (err) => {
            Swal.default.fire('Error',err.error.msg,'error');
          });
    
  }

  cambiarImagen(event: any): any{
    const file = event.target.files[0];
    this.imagenSubir  = file;

    if(!file){
      return this.imgTemp = null;
    }

    //cargamos la imagen seleccionada para leerla como base64
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.imgTemp = reader.result;
    };
  }

  subirImagen(){
    //el simbolo de admiración al final de una variable indica que nunca va a se undefined
    this.fileUploadService.actualizaFoto(this.imagenSubir, 'usuarios', this.usuario.uid!)
      .then(img => {
        if(img){
          this.usuario.img = img;
          Swal.default.fire(
            {
              position: 'center',
              icon: 'success',
              title: 'Foto actualizada correctamente',
              showConfirmButton: false,
              timer: 2850
            }
          );//'Actualizado','Tu foto de fue actualizada correctamente','success');
        }
      } )
      .catch(err => {
        Swal.default.fire(
          {
            position: 'center',
            icon: 'error',
            title: 'No pudimos subir la imagen',
            showConfirmButton: false,
            timer: 2850
          }
        );
      });
  }

}

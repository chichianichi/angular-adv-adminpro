import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../services/settings.service';
import { SidebarService } from '../services/sidebar.service';

// @ts-ignore
declare function customInitFuntions();

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {
  

  //EL CONSTRUCTOR DEL SERVICIO SE INICIALIZA CUANDO SE INYECTA
  constructor(
    private settingService: SettingsService,
    private sidebarService: SidebarService
  ) { }

  ngOnInit(): void {
    customInitFuntions();
    this.sidebarService.cargarMenu();
  }

}

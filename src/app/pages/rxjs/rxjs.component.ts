import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
//los operators son como piezas de lego que se unene a un observable
import { retry,take, map, filter } from "rxjs/operators";

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy {
  public intervalSubs!: Subscription;

  constructor() { 
    

    // this.retornaObserblable()
    // .pipe(
    //   //reintenta hasta que el observable se finalice, recibimos el error y vuelve a ejecutar el observable
    //   retry(1)
    // )
    // .subscribe(
    //   (valor) => console.log('subs:', valor), 
    //   (err) => console.warn('Error:', err),
    //   () => console.info('obs terminado')
    // );

    this.intervalSubs = this.retornaIntervalo().subscribe(console.log);
  }

  ngOnDestroy(): void {
    //terminamos el observable, usado para observables que emiten muchos valores
    this.intervalSubs.unsubscribe();
  }

  retornaIntervalo(): Observable<number>{
    return interval(100)
    //dentro del pipe se ponen elementos en el orden que queremos que se ejecuten
            .pipe(
              //la funcion take define cuantos veces se va a correr el observable
              //take(10),
              //map es una función que recorrera los datos del obsevable
              map(valor => valor + 1),
              //sirve para filtrar elementos, si damos true nos retorna el valor que entra a la funcion
              filter( valor => (valor % 2 == 0) ? true: false),
            );
  }

  retornaObserblable(): Observable<number>{
    let i = -1;
    return new Observable<number>(observer => {
      
      const intervalo = setInterval(() => {
        i++;
        observer.next(i);
        if(i === 4){
          clearInterval(intervalo);
          observer.complete();
        }

        if(i === 2){
          observer.error('i llegó al valor de 2')
        }

      },1000);
    });
  }
}

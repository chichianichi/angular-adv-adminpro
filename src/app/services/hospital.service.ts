import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, mapTo } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Hospital } from '../models/hospital.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class HospitalService {

  constructor(
    private http: HttpClient,
  ) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  cargarHospitales(desde: number = 0){
    //puedes añadir la paginación en el back
    const url = `${base_url}/hospitales`
    return this.http.get<any>(url, this.headers)
              .pipe(
                map((resp: {ok: boolean, hospitales: Hospital[]}) => resp.hospitales)
              );
  }

  crearHospital(nombre: string){
    //puedes añadir la paginación en el back
    const url = `${base_url}/hospitales`;
    return this.http.post(url, {nombre}, this.headers);
  }

  actualzarHospital(_id: string, nombre: string){
    //puedes añadir la paginación en el back
    const url = `${base_url}/hospitales/${_id}`
    return this.http.put(url, {nombre}, this.headers);
  }

  borrarHospital(_id: string){
    //puedes añadir la paginación en el back
    const url = `${base_url}/hospitales/${_id}`
    return this.http.delete(url, this.headers);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Medico } from '../models/medico.model';

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  constructor(
    private http: HttpClient,
  ) { }

  cargarMedicos(){
     //puedes añadir la paginación en el back
     const url = `${base_url}/medicos`;
     return this.http.get<any>(url, this.headers)
               .pipe(
                 map((resp: {ok: boolean, medicos: Medico[]}) => resp.medicos)
               );
  }

  obtenerMedicoPorId(id: string){
    const url = `${base_url}/medicos/${id}`;
    return this.http.get<any>(url, this.headers)
               .pipe(
                 map((resp: {ok: boolean, medico: Medico}) => resp.medico)
               );
  }

  crearMedico(medico: { nombre: string, hospital: string }){
    //puedes añadir la paginación en el back
    const url = `${base_url}/medicos`;
    return this.http.post(url, medico, this.headers);
  }

  actualzarMedico(medico: Medico){
    //puedes añadir la paginación en el back
    const url = `${base_url}/medicos/${medico._id}`
    return this.http.put(url, medico, this.headers);
  }

  borrarMedico(_id: string){
    //puedes añadir la paginación en el back
    const url = `${base_url}/medicos/${_id}`
    return this.http.delete(url, this.headers);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

import { environment } from 'src/environments/environment';
import { CargarUsuario } from '../interfaces/cargar-usuarios.interface';

import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interfaces';

import { Usuario } from '../models/usuario.model';

declare const google: any;

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public usuario!: Usuario;

  constructor(
    private http: HttpClient,
    private router: Router,
    private ngZone: NgZone
  ) { 
    this.googleInit();
  }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get uid(): string{
    return this.usuario.uid || '';
  }

  get role(): 'ADMIN_ROLE' | 'USER_ROLE'{
    return this.usuario.role!;
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('menu');
    google.accounts.id.revoke('chichianichi85@gmail.com', () => {
      //NgZone es para cuando ejecutamos un procedimiento fuera de angular, como una
      //librería externa, con ngZone ejecutamos procesos de angular que no le pertenencen
      //como los de la librería de google por ejemplo
      this.ngZone.run(() =>  this.router.navigateByUrl('/login'));
    });
  }

  googleInit(){
    google.accounts.id.initialize({
      client_id: "677805982200-ir19sardc87u4mr3qvb88evp55sk62ib.apps.googleusercontent.com",
      callback: (response: any) => this.handleCredentialResponse(response)
    });
  }

  guardarLocalStorage(token: string, menu: any){
    localStorage.setItem('token', token);
    //localstorage solo guarda strings, el menu es un objeto y debemos convertirlo a json para almacenarlo
    localStorage.setItem('menu', JSON.stringify(menu));
  }

  handleCredentialResponse(response: any){
    //console.log("Encoded JWT ID token: " + response.credential);
    this.loginGoogle(response.credential)
      .subscribe(resp => {
        this.ngZone.run(() => this.router.navigateByUrl('/'));
      });
  }

  validarToken(): Observable<boolean>{
    return this.http.get(`${base_url}/login/renew`, {
      headers: {
        'x-token': this.token
      }
    }).pipe(
      map((resp: any) => {
       
        const { email, google, nombre, role,img = '', uid } = resp.usuario;
        this.usuario = new Usuario(nombre, email, '', img, google, role, uid);
        this.guardarLocalStorage(resp.token,resp.menu);
        return true;
      }),
      catchError(error => of(false))
    );
  }

  crearUsuario(formData: RegisterForm){
    return this.http.post(`${base_url}/usuarios`, formData)
                  .pipe(
                    //tap recibe la respuesta de la petición
                    tap((resp: any) => {
                      this.guardarLocalStorage(resp.token,resp.menu);
                    })
                  );
  }

  actualizarPerfil(data: {email: string, nombre: string, role: string}){
    data = {
      ...data,
      role: this.usuario.role || ''
     }
    return this.http.put(`${base_url}/usuarios/${this.uid}`, data, this.headers);
  }

  login(formData: LoginForm){
    return this.http.post(`${base_url}/login`, formData)
                  .pipe(
                    //tap recibe la respuesta de la petición
                    tap((resp: any) => {
                      this.guardarLocalStorage(resp.token,resp.menu);
                    })
                  );
  }

  loginGoogle(token: string){
    return this.http.post(`${base_url}/login/google`, { token })
                  .pipe(
                    //tap recibe la respuesta de la petición
                    tap((resp: any) => {
                      this.guardarLocalStorage(resp.token,resp.menu);
                    })
                  );
  }

  cargarUsuarios(desde: number = 0){
    const url = `${base_url}/usuarios?desde=${desde}`
    return this.http.get<CargarUsuario>(url, this.headers)
                      .pipe(
                        map((resp) => {
                          const usuarios = resp.usuarios.map(
                            usuario => new Usuario(
                                                    usuario.nombre, 
                                                    usuario.email, 
                                                    '', 
                                                    usuario.img, 
                                                    usuario.google, 
                                                    usuario.role,
                                                    usuario.uid
                                                  )
                          );
                          return {
                            usuarios,
                            total: resp.total
                          };
                        })
                      );
  }

  eliminarUsuarui(usuario: Usuario){
    const url = `${base_url}/usuarios/${usuario.uid}`;
    return this.http.delete<CargarUsuario>(url, this.headers);
  }

  guardarUsuario(usuario: Usuario){
    return this.http.put(`${base_url}/usuarios/${usuario.uid}`, usuario, this.headers);
  }
}

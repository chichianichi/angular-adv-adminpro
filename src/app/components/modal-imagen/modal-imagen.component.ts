import { Component, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.component.html',
  styles: [
  ]
})
export class ModalImagenComponent implements OnInit {
  public imagenSubir!: File;
  public imgTemp!: any;

  constructor(
    public modalImagenService: ModalImagenService,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
  }

  cerrarModal(){
    this.imgTemp = null;
    this.modalImagenService.cerrarModal();
  }

  cambiarImagen(event: any): any{
    const file = event.target.files[0];
    this.imagenSubir  = file;

    if(!file){
      return this.imgTemp = null;
    }

    //cargamos la imagen seleccionada para leerla como base64
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.imgTemp = reader.result;
    };
  }

  subirImagen(){
    const id = this.modalImagenService.id;
    const tipo = this.modalImagenService.tipo;
    //el simbolo de admiración al final de una variable indica que nunca va a se undefined
    this.fileUploadService.actualizaFoto(this.imagenSubir, tipo, id)
      .then(img => {
        if(img){
          Swal.fire(
            {
              position: 'center',
              icon: 'success',
              title: 'Foto actualizada correctamente',
              showConfirmButton: false,
              timer: 2850
            }
          );
          //mandamos un string para que se detecte un cambio en el observable
          this.modalImagenService.nuevaImagen.emit(img);
          this.cerrarModal();
        }
      } )
      .catch(err => {
        Swal.fire(
          {
            position: 'center',
            icon: 'error',
            title: 'No pudimos subir la imagen',
            showConfirmButton: false,
            timer: 2850
          }
        );
      });
  }

}

/**
 * Cuando usar clases y cuando usar interfaces?
 * si a lo largo del desarrollo necesitas implementar métodos de los objetos
 * usa clases.
 * 
 * De lo contrario si solo es para el mapeo usa interfaces, es más ligero
 */

interface _HospitalUser {
  _id: string,
  nombre: string;
  img: string

}

export class Hospital{
  constructor(
    public nombre: string,
    public _id?: string,
    public img?: string,
    public usuario?: _HospitalUser,
  ){}
}